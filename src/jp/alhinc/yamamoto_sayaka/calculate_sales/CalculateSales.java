package jp.alhinc.yamamoto_sayaka.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {

	public static void main(String[] args){

			//HashMap<K, V>{STRING TODO 自動生成されたメソッド・スタブ
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//branchNameMapというマップを作る（名前はわかりやすければなんでもOK)
		HashMap<String,String> nameMap = new HashMap<String,String>();
		//totalMapというマップを作る（名前はわかりやすければなんでもOK)
		HashMap<String,Long> totalMap = new HashMap<String,Long>();


//		支店定義ファイルの読み込みメソッド分け
		if(!lstFileRead(args[0], "branch.lst", "\\d{3}", nameMap, totalMap)){
			//下のreadという箱に入れるもの
			return;
		}


		//2-1支店定義ファイル読み込み
		//条件に当てはまるファイル名をリストの中身から検索して、ArrayList"fileList"に入れる
		//argsの中のファイルを1つずつリスト化
		File dir = new File(args[0]);
		File[] list = dir.listFiles();
		//リスト内のファイル１つ１つにifの条件を当てはめてArrayListに入れていく
		ArrayList<String>  fileList = new ArrayList<String>();
		//↑fileListというArrayListの箱を作る
		for(int i = 0; i < list.length; i++) {
			if(list[i].getName().matches("\\d{8}.rcd") && list[i].isFile() ) {
				fileList.add(list[i].getName());
			}
		}
		//エラー処理　連番処理
		for(int i = 0; i < fileList.size() -1; i++) {
			String str3 = fileList.get(i);
			String str4 = str3.replace(".rcd","");
			String str5 = fileList.get(i+1);
			String str6 = str5.replace(".rcd","");
			int nb4 = Integer.parseInt(str4);
			int nb6 = Integer.parseInt(str6);
			int minus = nb6 - nb4;
			if(minus != 1) {
				System.out.println("売上ファイル名が連番になっていません");
					return;
			}
		}

		//2-2集計
		//ArrayList"fileList"からファイルの中身を読み込み、リスト"lineList"に入れていく
		for(int i = 0; i < fileList.size(); i++) {
							//↑fileList内にある要素の数
			BufferedReader br = null;
			try{
				File file = new File(args[0],fileList.get(i));
				String fileName = fileList.get(i);
				FileReader fr = new FileReader(file);

				//エラー処理2-4売上ファイルの中身が3行以上でエラー
				br = new BufferedReader(fr);
				//lineListという箱を作る
				ArrayList<String> lineList = new ArrayList<String>();
				//lineListという箱にファイルの中身の行を入れていく
				String line;
				while((line = br.readLine()) != null) {
					lineList.add(line);
				}
				if(lineList.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				if(!lineList.get(1).matches("[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//エラー処理2-3支店に該当なかったらエラー
				if(!nameMap.containsKey(lineList.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				//"lineList"の要素[1]（売上額）に"istsalese"、"totalMap"のvalueに"mapsalse"という名前をつけた
				//valueを数値型Longに変換
				//17行でtotalMapのkeyをSring、valueをLongにしたから
				Long listsalse = Long.parseLong(lineList.get(1));
				Long mapsalse = totalMap.get(lineList.get(0));
											//↑()の中身はtotalMapのkey
				Long totalSum = listsalse + mapsalse;

				//エラー処理2-2合計金額が10桁超えたらエラー
				if(totalSum >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//21行で作った"totalMap"マップにkey（支店コード）とvalue（売上+支店の合計金額）を入れる
				//totalMapのkeyとvalueは既に46行で設定したが、keyが同じなのでvalueを更新できる
				totalMap.put(lineList.get(0),totalSum);
					//lineList.get(0)=支店コード、lineList.get(1)=売上額
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}


		//出力のメソッド分け
		if(!outPutResult(args[0], "branch.out", nameMap, totalMap)){
			//下のoutPutという箱に入れるもの
			return;
		}
	}


	//支店定義ファイルの読み込みメソッド
	static boolean lstFileRead(String dirPath, String fileName, String regex, HashMap <String, String>nameMap, HashMap <String, Long>totalMap){
		//1-2
		//"branch.lst"ファイルから読み込む
		BufferedReader br = null;
		try {
			File file = new File(dirPath,fileName);
			if(! file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
			//フォルダbranch.lst内の「001,札幌支店」をカンマで分ける
				String[] items = line.split(",");

				//エラー処理
				if(!items[0]. matches(regex) || items.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				//"branchNameMapにkey:支店コード、value:支店名を設定
				nameMap.put(items[0], items[1]);
				//売上額を０にする。key:支店コード、value:0に設定
				totalMap.put(items[0], 0L);
			}
		} catch(FileNotFoundException e) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	//出力のメソッド
	//"outPut"というメソッドをつくる
	static boolean outPutResult(String dirPath, String fileName, HashMap <String, String>nameMap, HashMap <String, Long>totalMap){
					   //(args[0]を☆受け取る箱,"branch.out"を受け取る箱,branchNameMapを受け取る箱,totalMapを受け取る箱)
		//3-1処理内容に記載のないエラー
			//args[0]に".branch.out"という名前のファイルを作りたい
			//支店コード、支店名合計金額の出力
		BufferedWriter bw = null;
		try{
			File totalSumFileWrite = new File(dirPath , fileName);
			FileWriter fw = new FileWriter(totalSumFileWrite);
			bw = new BufferedWriter(fw);
			for(String key : totalMap.keySet()){

				bw.write(key + "," + nameMap.get(key) + "," + totalMap.get(key));
				bw.newLine();
			}
		//bw.close();
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if(bw != null) {
					try {
						bw.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
			return true;
	}
}



